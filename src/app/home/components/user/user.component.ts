import { NetworkService } from '@core/services/network.service';
import { Component, Input, OnInit } from '@angular/core';
import { User } from '@core/models/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent {

  @Input() user: User;
  @Input() userIndex: number;

  get userId(): number {
    return this.networkService.isOnline ? this.user?.id : this.userIndex;
  }

  constructor(private networkService: NetworkService) { }
}
