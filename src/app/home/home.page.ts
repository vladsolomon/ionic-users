import { UsersService } from '@core';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/core/models/user';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  title = 'Users';
  users$!: Observable<User[]>;

  constructor(
    private usersService: UsersService,
  ) { }

  ionViewWillEnter(): void {
    this.getUsers();
  }

  getUsers() {
    this.users$ = this.usersService.getUsers();
  }
}
