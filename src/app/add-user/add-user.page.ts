import { NetworkService } from '@core/services/network.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { filter, map, mergeMap, take } from 'rxjs/operators';
import { User } from '@core/models/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '@core';
import { Subscription } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit, OnDestroy {

  user: User;
  sub$: Subscription = new Subscription();

  get isEdit(): boolean {
    return !!this.route.snapshot.params.id || !!this.user?.id;
  }

  userForm: FormGroup = this.formBuilder.group({
    firstName: ['', [Validators.required, Validators.maxLength(100)]],
    lastName: ['', [Validators.required, Validators.maxLength(100)]],
    email: ['', [Validators.required, Validators.email, Validators.maxLength(100)]],
    dateOfBirth: ['', [Validators.required]],
    emailVerified: [false]
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usersService: UsersService,
    private formBuilder: FormBuilder,
    public network: NetworkService,
  ) { }

  ngOnInit() {
    this.subscribeToRoute();
  }

  ngOnDestroy() {
    this.sub$.unsubscribe();
  }

  subscribeToRoute() {
    this.sub$ = this.route.params.pipe(
      filter((params: Params) => params.id),
      map((params: Params) => params.id),
      mergeMap((id: number) => this.usersService.getUser(id))
    ).subscribe((res: User) => {
      this.user = res;
      this.fillForm();
    });
  }

  fillForm() {
    Object.entries(this.userForm.controls).forEach(([field, control]) => {
      if (this.user[field]) {
        control.patchValue(this.user[field]);
      }
    });
  }

  submit() {
    if (this.isEdit && this.userForm.valid) {
      const sub = this.usersService.updateUser({ ...this.userForm.value, id: this.user.id }).subscribe(res => {
        console.log('updateUser', res);
        this.router.navigate(['/']);
        this.userForm.reset();
      }, err => {
          console.error(err);
      });
      this.sub$.add(sub);
    }
    if (!this.isEdit) {
      const createDate = moment(new Date()).format('YYYY-MM-DD');
      if (this.userForm.valid) {
        const sub = this.usersService.createUser({ ...this.userForm.value, createDate }).subscribe(res => {
          console.log('createUser', res);
          this.router.navigate(['/']);
          this.userForm.reset();
        }, err => {
          console.error(err);
      });
        this.sub$.add(sub);
      }
    }
  }

  deleteUser(userId: number) {
    this.usersService.deleteUser(userId).pipe(take(1)).subscribe();
    this.router.navigate(['/']);
  }
}
