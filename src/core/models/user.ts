export interface User {
  id: number;
  createDate: Date | string;
  dateOfBirth: Date | string;
  email: string;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
}
