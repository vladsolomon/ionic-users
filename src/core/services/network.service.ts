import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { Platform } from '@ionic/angular';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  public isOnline = false; // true
  public connected$: Subject<void> = new Subject();

  constructor(
    private network: Network,
    private platform: Platform,
  ) {
    this.subscribeToNetwork();
  }

  private subscribeToNetwork() {
    if (this.platform.is('hybrid')) {
      this.network.onDisconnect().subscribe(() => {
        this.isOnline = false;
      });
      this.network.onConnect().subscribe(() => {
        this.isOnline = true;
        this.connected$.next();
      });
    }

    if (this.platform.is('desktop') || this.platform.is('mobileweb')) {
      window.addEventListener('offline', () => {
        this.isOnline = false;
      }, false);
      window.addEventListener('online', () => {
        this.isOnline = true;
        this.connected$.next(); // subscribe and make queued requests
      }, false);
    }
  }
}
