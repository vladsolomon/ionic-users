import { NetworkService } from './network.service';
import { StorageService } from './storage.service';
import { Observable, of } from 'rxjs';
import { BASE_URL } from 'src/core/api.factory';
import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  readonly usersKey = 'users';
  readonly queueKey = `${this.usersKey}_offline`;
  isLocalUsersExist: boolean;

  constructor(
    @Inject(BASE_URL) private baseUrl: string,
    private http: HttpClient,
    private storage: StorageService,
    private networkService: NetworkService,
  ) {
    this.isLocalUsersExist = !!this.storage.getItem(this.usersKey);
    this.subscribeToConnection();
  }

  getUsers(): Observable<User[]> {
    if (this.isLocalUsersExist) {
      return this.getLocalUsers();
    }
    return this.http.get<User[]>(`${this.baseUrl}/users`)
      .pipe(map((users: User[]) => {
        this.storage.setItem(this.usersKey, users);
        this.isLocalUsersExist = true;
        return users;
      }));
  }

  getUser(id: number): Observable<User> {
    if (this.networkService.isOnline) {
      return this.http.get<User>(`${this.baseUrl}/users/${id}`);
    } else {
      // id is index in local mode
      return of(this.getLocalUsersSync()[id]);
    }
  }

  createUser(user: User): Observable<User> {
    if (this.networkService.isOnline) {
      return this.http.post<User>(`${this.baseUrl}/users`, user);
    } else {
      const users = this.getLocalUsersSync();
      users.push(user);
      this.storage.setItem(this.usersKey, users);
      this.addToQueue('createUser', user);
      return of(user);
    }
  }

  updateUser(user: User): Observable<User> {
    if (this.networkService.isOnline) {
      return this.http.patch<User>(`${this.baseUrl}/users/${user.id}`, user);
    } else {
      const users = this.getLocalUsersSync();
      const userIndex = users.map(u => u.id).indexOf(user.id);
      users[userIndex] = user;
      this.storage.setItem(this.usersKey, users);
      this.addToQueue('updateUser', user);
      return of(user);
    }
  }

  deleteUser(id: number): Observable<void> {
    if (this.networkService.isOnline) {
      return this.http.delete<void>(`${this.baseUrl}/users/${id}`);
    } else {
      const users = this.getLocalUsersSync();
      const userIndex = users.map(u => u.id).indexOf(id);
      users.splice(userIndex, 1);
      this.storage.setItem(this.usersKey, users);
      this.addToQueue('deleteUser', id);
      return of();
    }
  }

  getLocalUsers(): Observable<User[]> {
    return of(this.getLocalUsersSync());
  }
  getLocalUsersSync(): User[] {
    return this.storage.getItem(this.usersKey);
  }

  addToQueue(method: string, data: any) {
    const queueKey = this.queueKey;
    let queue = this.storage.getItem(queueKey);
    if (!queue) {
      queue = [];
    }
    queue.push({ method, data });
    this.storage.setItem(this.queueKey, queue);
  }

  subscribeToConnection() {
    this.networkService.connected$.subscribe(() => {
      const queue = this.storage.getItem(this.queueKey);
      if (queue?.length) {
        queue.forEach(({ method, data }, index: number) => {
          setTimeout(() => {
            switch (method) {
              case 'createUser':
                this.createUser(data).pipe(take(1)).subscribe();
                break;
              case 'updateUser':
                this.updateUser(data).pipe(take(1)).subscribe();
                break;
              case 'deleteUser':
                this.deleteUser(data).pipe(take(1)).subscribe();
                break;
              default:
                console.warn(`Unknown method ${method}`);
                break;
            }
          }, index * 100);
        });
        this.storage.removeItem(this.queueKey);
      }
    });
  }

}
