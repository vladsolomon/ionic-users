import { Network } from '@ionic-native/network/ngx';
import { StorageService } from './services/storage.service';
import { UsersService } from './services/users.service';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NetworkService } from './services/network.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    UsersService,
    StorageService,
    Network,
    NetworkService,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule already loaded; import in root module only.');
    }
  }
  static forRoot(): ModuleWithProviders<CoreModule> {
    return { ngModule: CoreModule };
  }
}
