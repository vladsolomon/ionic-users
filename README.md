# Ionic-angular test app "Users"

Written by Vlad Samoylenko `vladsolomon29@gmail.com`

## How to begin work

- make shure that you already install `npm` and `nodejs`
- run `npm install` to install all node modules
- run `npm run start` to execute app in browser mode

## Build

[Android SDK](https://developer.android.com/studio) is required to build android app

- once update your local .bashrc file using `./.bashrc` on linux os
- run `build:android` to build and sign android app
